// Implement a map data structure using: linked list
#include <iostream>

using namespace std;

class Node{
public:
    Node* next;
    int key;
    int value;
    Node(int key, int value){
        this->key = key;
        this->value = value;
        this->next = NULL;
    }
    Node(int key, int value, Node* next){
        this->key = key;
        this->value = value;
        this->next = next;
    }
};


class Map{
public:
    Node *header;
    int sizeMap;
    Map(){
        header = NULL;
        sizeMap = 0;
    }

    int get(int k){
        Node* p = header;
        while(p != NULL){
            if(p->key == k){
                return p->value;
            }
            p = p->next;
        }
        cout << "key does not exist!";
        return NULL;
    }

    int put(int k, int v){
        if(header == NULL){
            header = new Node(k, v);
            sizeMap++;
            return NULL;
        }else{
            Node* prev = NULL;
            Node *p = header;
            while(p != NULL){
                if(p->key == k){
                    int oldValue = p->value;
                    p->value = v;
                    return oldValue;
                }
                prev = p;
                p = p->next;
            }
            Node* temp = new Node(k, v);
            prev->next = temp;
            sizeMap++;
            return NULL;
        }
    }

    int remove(int k){
        if(header == NULL){
            cout << "Key " << k << " does not exist!";
            return NULL;
        }
        if(header->key == k){
            Node *p = header;
            header = header->next;
            int v = p->value;
            delete p;
            sizeMap--;
            return v;
        }
        Node *prev = header;
        Node *p = header->next;
        while(p != NULL){
            if(p->key == k){
                prev->next = p->next;
                int v = p->value;
                delete p;
                sizeMap--;
                return v;
            }
            p = p->next;
            prev = prev->next;
        }
        cout << "Key " << k << " does not exist!";
        return NULL;
    }

    int size(){
        return sizeMap;
    }

    bool isEmpty(){
        return header == NULL;
    }
    void show(){
        Node *p = header;
        while(p!= NULL){
            cout << "(" << p->key << ", " << p->value << ") -> ";
            p = p->next;
        }
        cout << "null" << endl;
    }
};


int main()
{
    Map map;
    // check empty
    if(map.isEmpty()) cout << "Map is empty";

    map.put(5, 1);
    map.show();
    cout << "size = " << map.size() << endl;

    map.put(6,2);
    map.show();
    cout << map.size() << endl;

    map.put(5,3);
    map.show();
    cout << map.size() << endl;

    // check empty
    cout << "value at key 5 =  " << map.get(5) << endl;
    cout << "map is empty? Answer: " << map.isEmpty() << endl;

    for(int i = 11; i < 20; i++){
        map.put(i,i);
    }
    map.show();
    cout << "size = " << map.size() << endl;

    // remove an element in the map;
    map.remove(11);
    map.show();
    cout << "size = " << map.size() << endl;

    // remove header
    map.remove(5);
    map.show();
    cout << "size = " << map.size() << endl;
    // remove trailer
    map.remove(19);
    map.show();
    cout << "size = " << map.size() << endl;
}
