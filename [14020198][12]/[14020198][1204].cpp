// Implement a dictionary data structure using: Linked list
#include <iostream>
#include <vector>
using namespace std;

class Node{
public:
    Node* next;
    int key;
    int value;
    Node(int key, int value){
        this->key = key;
        this->value = value;
        this->next = NULL;
    }
    Node(int key, int value, Node* next){
        this->key = key;
        this->value = value;
        this->next = next;
    }
};


class Dictionary{
public:
    Node *header;
    int sizeDic;
    Dictionary(){
        header = NULL;
        sizeDic = 0;
    }

    Node* find(int k){
        Node* p = header;
        while(p != NULL){
            if(p->key == k){
                cout << "(" << k << ", " << p->value << ")" << endl;
                return p;
            }
            p = p->next;
        }
        cout << "Khong tim thay gia tri nao thoa man";
        return NULL;
    }

    vector<Node*> findAll(int k){
        Node* p = header;
        vector<Node*> temp;
        while(p!= NULL){
            if(p->key == k){
                cout << "(" << k << ", " << p->value << ") ";
                temp.push_back(p);
            }
            p = p->next;
        }
        if(!temp.size()) cout << "Khong tim thay gia tri nao thoa man!";
        return temp;
    }

    Node* insert(int k, int v){
        if(header == NULL){
            header = new Node(k, v);
            sizeDic++;
            return header;
        }else{
            Node *p = header;
            while(p->next != NULL){
                p = p->next;
            }
            Node* temp = new Node(k, v);
            p->next = temp;
            sizeDic++;
            return temp;
        }
    }

    Node* remove(Node* a){
        if(a == NULL || header == NULL){
            cout << "key does not exist!";
            return NULL;
        }
        int k = a->key;
        if(header->key == k){
            Node *p = header;
            header = header->next;
            int v = p->value;
            delete p;
            sizeDic--;
            cout << "(" << k << ", " << v  << ")" << endl;
            return new Node(k, v);
        }
        Node *prev = header;
        Node *p = header->next;
        while(p != NULL){
            if(p->key == k){
                prev->next = p->next;
                int v = p->value;
                delete p;
                sizeDic--;
                cout << "(" << k << ", " << v  << ")" << endl;
                return new Node(k, v);
            }
            p = p->next;
            prev = prev->next;
        }
        cout << "Key " << k << " is not exist!";
        return NULL;
    }

    int size(){
        return sizeDic;
    }

    bool isEmpty(){
        return header == NULL;
    }
    void show(){
        Node *p = header;
        while(p!= NULL){
            cout << "(" << p->key << ", " << p->value << ") -> ";
            p = p->next;
        }
        cout << "null" << endl;
    }
};


int main()
{
    Dictionary dic;
    if(dic.isEmpty()) cout << "empty dictionary";
    else cout << "not empty dictionary";
    dic.insert(5, 6);
    dic.show();
    dic.insert(5,7);
    dic.show();
    dic.insert(5,8);
    dic.show();
    dic.insert(6,7);
    dic.show();
    if(dic.isEmpty()) cout << "empty dictionary";
    else cout << "not empty dictionary";
    dic.find(5);
    dic.find(6);
    dic.findAll(5);
    cout << "remove = "; dic.remove(dic.find(5));
    dic.show();
    cout << "remove = "; dic.remove(new Node(5,7));
    dic.show();
}
