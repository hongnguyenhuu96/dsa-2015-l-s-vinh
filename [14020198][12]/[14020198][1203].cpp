// Implement a map data structure using: linear probing
#include <iostream>
#include <cstdlib>

using namespace std;

#define init_sizeTable 4

class Node{
public:
    int key;
    int value;
    Node(int key, int value){
        this->key = key;
        this->value = value;
    }
};

// linear probing
class HashMap{
public:
    Node** hTable;
    int sizeMap;
    int sizeTable;
    HashMap(){
        this->sizeMap = 0;
        this->sizeTable = 4; // 4 is default sizetable;
        this->hTable = new Node*[sizeTable];
        for(int i = 0; i < sizeTable; i++){
            hTable[i] = NULL;
        }
    }
    ~HashMap(){
        for(int i = 0; i < sizeTable; i++){
            delete hTable[i];
        }
        delete[] hTable;
    }
    HashMap(int sizeTable){
        this->sizeMap = 0;
        this->sizeTable = sizeTable;
        this->hTable = new Node*[sizeTable];
        for(int i = 0; i < sizeTable; i++){
            hTable[i] = NULL;
        }
    }
    int size(){
        return sizeMap;
    }
    bool isEmpty(){
        return sizeMap == 0;
    }
    int hash(int k){
        return k%sizeTable;
    }

    int put(int k, int v){
        if(sizeMap > sizeTable/2) resize(sizeTable*2);
        int i = hash(k);
        // find nearest position to put
        while(hTable[i] != NULL){
            // if key exist then change the current value to v and return oldValue
            if(hTable[i]->key == k){
                int oldValue = hTable[i]->value;
                hTable[i]->value = v;
                return oldValue;
            }
            i = (i+1)%sizeTable;
        }
        // if key does not exist, create a new Node and put into the nearest NULL position
        hTable[i] = new Node(k, v);
        sizeMap++;
    }

    int get(int k){
        for(int i = hash(k); hTable[i] != NULL; i = (i + 1)% sizeTable){
            if(hTable[i]->key == k){
                return hTable[i]->value;
            }
        }
        return NULL;
    }


    int remove(int k){
        int i = hash(k);
        while(hTable[i] != NULL){
            if(hTable[i]->key != k){
                i = (i + 1)%sizeTable;
            } else break;
        }
        // hTable[i]->key == k || hTable[i] == NULL
        // if not found
        if(hTable[i] == NULL){
            cout << "key " << k << " does not exist!";
            return NULL;
        }
        // if found
        int oldValue = hTable[i]->value;
        delete hTable[i];
        hTable[i] = NULL;
        i = (i + 1)%sizeTable;
        while(hTable[i] != NULL){
            int keyRehash = hTable[i]->key;
            int valueRehash = hTable[i]->value;
            delete hTable[i];
            hTable[i] = NULL;
            sizeMap--;
            put(keyRehash, valueRehash);
            i = (i + 1)%sizeTable;
        }
        sizeMap--;
        if (sizeMap > 0 && sizeMap <= sizeTable/4) resize(sizeTable/2);
        return oldValue;
    }

    void resize(int newTableSize){
        HashMap *temp = new HashMap(newTableSize);
        for(int i = 0; i < sizeTable; i++){
            if(hTable[i] != NULL){
                temp->put(hTable[i]->key, hTable[i]->value);
            }
        }
        HashMap* p = this;
        this->sizeMap = temp->sizeMap;
        this->sizeTable = temp->sizeTable;
        this->hTable = temp->hTable;
    }
    void show(){
        for(int i = 0; i < sizeTable; i++){
            if(hTable[i] != NULL){
                cout << "(" << hTable[i]->key << ", " << hTable[i]->value << ")->";
            }
            else cout << "NULL->";
        }
        cout << endl;
    }
};

int main()
{
    HashMap map;
    // check empty
    if(map.isEmpty()) cout << "Map is empty" << endl;
    map.put(5,1);
    map.show();
    cout << "size = " << map.size() << endl;

    map.put(6,2);
    map.show();
    cout << map.size() << endl;

    map.put(5,3);
    map.show();
    cout << map.size() << endl;

    // check empty
    cout << "value at key 5 =  " << map.get(5) << endl;
    cout << "map is empty? Answer: " << map.isEmpty() << endl;

    for(int i = 11; i < 25; i++){
        cout << "put (" << i << ", " << i << ")" << endl;
        map.put(i,i);
        map.show();
        cout << "size = " << map.size() << endl << endl;
    }
    map.show();
    cout << "size = " << map.size() << endl;

    // remove an element in the map;
    cout << "remove 11" << endl;
    map.remove(11);
    map.show();
    cout << "size = " << map.size() << endl;

    // remove header
    cout << "remove 5" << endl;
    map.remove(5);
    map.show();
    cout << "size = " << map.size() << endl;
    // remove trailer
    cout << "remove 19" << endl;
    map.remove(19);
    map.show();
    cout << "size = " << map.size() << endl;
    return 0;
}
