// Implement a dictionary data structure using: Hash table
#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;
#define TABLE_SIZE 13

class Node{
public:
    Node* next;
    int key;
    int value;
    Node(int key, int value){
        this->key = key;
        this->value = value;
        this->next = NULL;
    }
    Node(int key, int value, Node* next){
        this->key = key;
        this->value = value;
        this->next = next;
    }
};

// separate chaining
class HashDic{
public:
    Node** hTable;
    int sizeDic;
    HashDic(){
        hTable = new Node*[TABLE_SIZE];
        for(int i = 0; i < TABLE_SIZE; i++){
            hTable[i] = NULL;
        }
        sizeDic = 0;
    }

    ~HashDic(){
        for(int i = 0; i < TABLE_SIZE; i++){
            Node* temp1 = hTable[i];
            while(temp1 != NULL){
                Node* temp2 = temp1;
                temp1 = temp1->next;
                delete temp2;
            }
            delete temp1;
        }
        delete[] hTable;
    }

    int hash(int k){
        return k % TABLE_SIZE;
    }

    int size(){
        return sizeDic;
    }
    bool isEmpty(){
        return sizeDic == 0;
    }
    Node* insert(int k, int v){
        int i = hash(k);
        if(hTable[i] == NULL){
            hTable[i] = new Node(k, v);
            sizeDic++;
            return hTable[i];
        }else{
            Node *p = hTable[i];
            while(p->next != NULL){
                p = p->next;
            }
            Node* temp = new Node(k, v);
            p->next = temp;
            sizeDic++;
            return temp;
        }
    }

    Node* find(int k){
        int i = hash(k);
        Node* p = hTable[i];
        while(p != NULL){
            if(p->key == k){
                cout << "(" << p->key << ", " << p->value << ")" << endl;
                return p;
            }
            p = p->next;
        }
        cout << "key does not exist!";
        return NULL;
    }

    vector<Node*> findAll(int k){
        vector<Node*> temp;
        int i = hash(k);
        Node* p = hTable[i];
        while(p != NULL){
            if(p->key == k){
                cout << "(" << p->key << ", " << p->value << ") ";
                temp.push_back(p);
            }
            p = p->next;
        }
        cout << endl;
        if(!temp.size())
            cout << "key does not exist!";
        return temp;
    }

    Node* remove(Node* a){
        if(a == NULL){
            cout << "the data which you want to remove does not exist!";
            return NULL;
        }
        int k = a->key;
        int i = hash(k);
        if(hTable[i] == NULL){
            cout << "Key " << k << " is not exist!";
            return NULL;
        }
        if(hTable[i]->key == k){
            Node *p = hTable[i];
            hTable[i] = hTable[i]->next;
            int v = p->value;
            delete p;
            sizeDic--;
            cout << "(" << k << ", " << v  << ")" << endl;
            return new Node(k, v);
        }
        Node *prev = hTable[i];
        Node *p = hTable[i]->next;
        while(p != NULL){
            if(p->key == k){
                prev->next = p->next;
                int v = p->value;
                delete p;
                sizeDic--;
                cout << "(" << k << ", " << v  << ")" << endl;
                return new Node(k, v);
            }
            p = p->next;
            prev = prev->next;
        }
        cout << "Key " << k << " is not exist!";
        return NULL;
    }

    void show(){
        cout << endl << "/---------------------------/" << endl;
        for(int i = 0; i < TABLE_SIZE; i++){
            cout << i << ": ";
            Node* p = hTable[i];
            while(p!= NULL){
                cout << "(" << p->key << ", " << p->value << ") -> ";
                p = p->next;
            }
            cout << "null" << endl;
        }
        cout << "/---------------------------/" << endl;
    }
};


int main()
{
    HashDic dic;
    if(dic.isEmpty()) cout << "empty dictionary";
    else cout << "not empty dictionary";
    dic.insert(5, 6);
    dic.show();
    dic.insert(5,7);
    dic.show();
    dic.insert(5,8);
    dic.show();
    dic.insert(6,7);
    dic.show();
    if(dic.isEmpty()) cout << "empty dictionary" << endl;
    else cout << "not empty dictionary" << endl;
    dic.find(5);
    dic.find(6);
    dic.findAll(5);
    cout << "remove = "; dic.remove(dic.find(5));
    dic.show();
    cout << "remove = "; dic.remove(new Node(5,7));
    dic.show();
}
