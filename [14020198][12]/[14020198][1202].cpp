// Implement a map data structure using: separate chaining method
#include <iostream>
#include <cstdlib>

using namespace std;
#define TABLE_SIZE 13

class Node{
public:
    Node* next;
    int key;
    int value;
    Node(int key, int value){
        this->key = key;
        this->value = value;
        this->next = NULL;
    }
    Node(int key, int value, Node* next){
        this->key = key;
        this->value = value;
        this->next = next;
    }
};

// separate chaining
class HashMap{
public:
    Node** hTable;
    int sizeMap;
    HashMap(){
        hTable = new Node*[TABLE_SIZE];
        for(int i = 0; i < TABLE_SIZE; i++){
            hTable[i] = NULL;
        }
        sizeMap = 0;
    }

    ~HashMap(){
        for(int i = 0; i < TABLE_SIZE; i++){
            Node* temp1 = hTable[i];
            while(temp1 != NULL){
                Node* temp2 = temp1;
                temp1 = temp1->next;
                delete temp2;
            }
            delete temp1;
        }
        delete[] hTable;
    }

    int hash(int k){
        return k % TABLE_SIZE;
    }

    int size(){
        return sizeMap;
    }
    bool isEmpty(){
        return sizeMap == 0;
    }
    int put(int k, int v){
        int i = hash(k);
        if(hTable[i] == NULL){
            hTable[i] = new Node(k, v);
            sizeMap++;
            return NULL;
        }else{
            Node* prev = NULL;
            Node *p = hTable[i];
            while(p != NULL){
                if(p->key == k){
                    int oldValue = p->value;
                    p->value = v;
                    return oldValue;
                }
                prev = p;
                p = p->next;
            }
            Node* temp = new Node(k, v);
            prev->next = temp;
            sizeMap++;
            return NULL;
        }
    }

    int get(int k){
        int i = hash(k);
        Node* p = hTable[i];
        while(p != NULL){
            if(p->key == k){
                return p->value;
            }
            p = p->next;
        }
        return NULL;
    }

    int remove(int k){
        int i = hash(k);
        if(hTable[i] == NULL){
            cout << "Key " << k << " is not exist!";
            return NULL;
        }
        if(hTable[i]->key == k){
            Node *p = hTable[i];
            hTable[i] = hTable[i]->next;
            int v = p->value;
            delete p;
            sizeMap--;
            return v;
        }
        Node *prev = hTable[i];
        Node *p = hTable[i]->next;
        while(p != NULL){
            if(p->key == k){
                prev->next = p->next;
                int v = p->value;
                delete p;
                sizeMap--;
                return v;
            }
            p = p->next;
            prev = prev->next;
        }
        cout << "Key " << k << " is not exist!";
        return NULL;
    }

    void show(){
        cout << endl << "/---------------------------/" << endl;
        for(int i = 0; i < TABLE_SIZE; i++){
            cout << i << ": ";
            Node* p = hTable[i];
            while(p!= NULL){
                cout << "(" << p->key << ", " << p->value << ") -> ";
                p = p->next;
            }
            cout << "null" << endl;
        }
        cout << "/---------------------------/" << endl;
    }
};

int main()
{
    HashMap map;
    // check empty
    if(map.isEmpty()) cout << "Map is empty";

    map.put(5, 1);
    map.show();
    cout << "size = " << map.size() << endl;

    map.put(6,2);
    map.show();
    cout << map.size() << endl;

    map.put(5,3);
    map.show();
    cout << map.size() << endl;

    // check empty
    cout << "value at key 5 =  " << map.get(5) << endl;
    cout << "map is empty? Answer: " << map.isEmpty() << endl;

    for(int i = 11; i < 25; i++){
        map.put(i,i);
        cout << "put (" << i << ", " << i << ")";
        map.show();
        cout << "size = " << map.size() << endl << endl;
    }
    map.show();
    cout << "size = " << map.size() << endl;

    // remove an element in the map;
    map.remove(11);
    map.show();
    cout << "size = " << map.size() << endl;

    // remove header
    map.remove(5);
    map.show();
    cout << "size = " << map.size() << endl;
    // remove trailer
    map.remove(19);
    map.show();
    cout << "size = " << map.size() << endl;
    return 0;
}
