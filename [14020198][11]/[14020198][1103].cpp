#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

struct LongestAt{
    int value = 0;
    string sequence = "";
    void set(int value1, string sequence1){
        value = value1;
        sequence = sequence1;
    }
};


float findlongestIncreasingSubsequence(float *sequence, LongestAt *longestAt, int n);
float* readFile(const char* fileName, int &n);

int main(){
    int n = 0;
    float *sequence = readFile("[14020198][1103].txt", n);
    LongestAt *longestAt = new LongestAt[n];
    int i =  findlongestIncreasingSubsequence(sequence, longestAt, n);
    cout << longestAt[i].sequence;
}


float* readFile(const char* fileName, int &n){
    ifstream file(fileName);
    file >> n;
    float *sequence = new float[n];
    float temp;
    int i = 0;
    while(!file.eof()){
        file >> temp;
        if(file){
            sequence[i] = temp;
            i++;
        }
    }
    return sequence;
}

float findlongestIncreasingSubsequence(float *sequence, LongestAt *longestAt, int n){
    for(int j = 0; j < n; j++){
        int maxLength = 0;
        int pos = j;
        for(int i = 0; i < j; i++){
            if(sequence[i] < sequence[j] && maxLength < longestAt[i].value){
                    maxLength = longestAt[i].value;
                    pos = i;
            }
        }
        longestAt[j].value = maxLength + 1;
        stringstream ss;
        if(longestAt[pos].sequence != "")
            ss << longestAt[pos].sequence << " ";
        ss << j + 1;
        longestAt[j].sequence += ss.str();
    }
    int max = 0;
    int pos = 0;
    for(int i = 0; i < n; i++){
        if(max < longestAt[i].value){
            max = longestAt[i].value;
            pos = i;
        }
    }
    return pos;
}
