#include <iostream>
#include <fstream>


using namespace std;

struct MaxAt{
    float value;
    int root;
    void set(float value1, int root1){
        value = value1;
        root = root1;
    }
};

float* readFile(const char* fileName, int &n);
float findMaxSubsequence(float *sequence, int n, MaxAt *maxAt);

int main(){
    int n = 0;
    float *sequence = readFile("[14020198][1102].txt", n);
    MaxAt *maxAt = new MaxAt[n];
    maxAt[0].set(sequence[0],0);
    int i =  findMaxSubsequence(sequence, n, maxAt);
    cout << "max = " << maxAt[i].value << endl;
    cout << "from: " << maxAt[i].root + 1 << " -> " << i + 1;
}


float* readFile(const char* fileName, int &n){
    ifstream file(fileName);
    file >> n;
    float *sequence = new float[n];
    float temp;
    int i = 0;
    while(!file.eof()){
        file >> temp;
        if(file){
            sequence[i] = temp;
            i++;
        }
    }
    return sequence;
}

float findMaxSubsequence(float *sequence, int n, MaxAt *maxAt){
    for(int i = 1; i < n; i++){
        if(maxAt[i - 1].value + sequence[i] > sequence[i]){
            maxAt[i].set(maxAt[i - 1].value + sequence[i], maxAt[i - 1].root);
        } else {
            maxAt[i].set(sequence[i], i);
        }
    }
    float max = 0;
    int pos;
    for(int i = 0; i < n; i++){
        if(maxAt[i].value > max){
            max = maxAt[i].value;
            pos = i;

        }
    }
    return pos;
}
