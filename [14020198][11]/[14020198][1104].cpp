#include <iostream>
#include <fstream>

using namespace std;

#define COST_INSERT 1
#define COST_DELETE 1
#define COST_REPLACE 1

void readFile(const char* fileName, string &s1, string &s2){
    ifstream file(fileName);
    getline(file, s1);
    getline(file, s2);
}

int computeMinimunOperate(string s1, string s2, int i, int j){
    if(i == 0) return j;
    if(j == 0) return i;
    if(s1[i] == s2[j]){
        return computeMinimunOperate(s1, s2, i - 1, j - 1);
    }else{
        int case1 = computeMinimunOperate(s1, s2, i, j - 1) + COST_INSERT;
        int case2 = computeMinimunOperate(s1, s2, i - 1, j) + COST_DELETE;
        int case3 = computeMinimunOperate(s1, s2, i - 1, j - 1) + COST_REPLACE;
        return min(min(case1,case2),case3);
    }
}


int main(){
	string s1, s2;
    readFile("[14020198][1104].txt", s1, s2);
    cout << computeMinimunOperate(s1, s2, s1.length(), s2.length());

}
