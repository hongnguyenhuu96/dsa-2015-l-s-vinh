#include <iostream>
#include <fstream>
using namespace std;

int* readFile(const char* fileName, int &n);
void findSubsequences(int *sequence, bool *check, int n, int &sum, int startAt);

int main()
{
    int n;
    int *sequence = readFile("[14020198][1101].txt", n);
    bool *check = new bool[n];
    for(int i = 0; i < n; i++){
        check[i] = false;
    }
    int sum = 0;
    findSubsequences(sequence, check, n, sum, 0);
}


int* readFile(const char* fileName, int &n){
    ifstream file(fileName);
    file >> n;
    int *a = new int[n];
    int i = 0;
    int temp;
    while(!file.eof()){
        file >> temp;
        if(file){
            a[i] = temp;
            i++;
        }
    }
    return a;
}

void findSubsequences(int *sequence, bool *check, int n, int &sum, int startAt){
    if(sum == 1000){
        for(int i = 0; i < n; i++){
            if(check[i]) cout <<  i + 1 << " ";
        }
        cout << endl;
        return ;
    }

    for(int i = startAt; i < n; i++){
        if(check[i] == false){
            check[i] = true;
            sum += sequence[i];
            findSubsequences(sequence, check, n, sum, i + 1);
            check[i] = false;
            sum -= sequence[i];
        }
    }
}
