#include <iostream>
#include <fstream>
using namespace std;

int* readFile(const char* fileName, int &x, int &n);
bool binarySearch(int *a,int x, int left, int right);


int main()
{
	int x, n;
	int *a = readFile("numbers.txt", x, n);
    if(binarySearch(a, x, 0, n - 1)){
    	cout << "YES";
    } else{
    	cout << "NO";
    }
}



int* readFile(const char* fileName, int &x, int &n){
	ifstream file(fileName);
	file >> x >> n;
	int *p = new int[n];
	int i = 0;
	while(!file.eof()){
		file >> p[i];
		i++;
	}
	return p;
}

bool binarySearch(int *a,int x, int left, int right){
    int mid = (left + right)/2;
	if(left <= right){
		if(a[mid] < x){
			return binarySearch(a, x, mid + 1, right);
		}
		else if(a[mid] > x){
			return binarySearch(a, x, left, mid - 1);
		}
		else {
			return true;
		}
	}
	return false;
}
