#include <iostream>
#include <fstream>
#include <ctime>

using namespace std;

struct Node{
    int key;
    Node* left;
    Node* right;
};
Node* readFile(const char* fileName, int &n, int &x);
Node* insertNewNode(Node *root, int temp);
Node* creatNewNode(int key);
void printTree(Node *root);
bool find(Node* root, const int &keyFind);

int main(){
    int n, x;
    Node* root = readFile("numbers.txt", n, x);
    printTree(root);
    cout << endl;
    if(find(root, x)){
        cout << "YES";
    }
    else{
        cout << "NO";
    }
}

Node* readFile(const char* fileName, int &n, int &x){
    ifstream file(fileName);
    file >> x >> n;
    Node *root = NULL;
    while(!file.eof()){
        int temp;
        file >> temp;
        if(file){
            root = insertNewNode(root, temp);
        }
    }
    return root;
}

Node* insertNewNode(Node *root, int temp){
    if(root == NULL){
        root = creatNewNode(temp);
    }
    else{
        Node *p = root;
        Node *beforeP;
        char c;
        while(p != NULL){
            if(temp <= p->key){
                c = 'l';
                beforeP = p;
                p = p->left;
            }
            else{
                c = 'r';
                beforeP = p;
                p = p->right;
            }
        }
        if(c == 'l') beforeP->left = creatNewNode(temp);
        if(c == 'r') beforeP->right = creatNewNode(temp);
    }
    return root;
}

Node* creatNewNode(int key){
    Node* temp = new Node();
    temp->key = key;
    temp->left = NULL;
    temp->right = NULL;
}


void printTree(Node *root){
    if(root!= NULL){
        printTree(root->left);
        printTree(root->right);
        cout << root -> key << " ";
    }
}

bool find(Node* root, const int &keyFind){
    if(root!= NULL){
        if(root->key > keyFind){
            return find(root->left, keyFind);
        }else if( root ->key < keyFind){
            return find(root->right, keyFind);
        }else{
            return true;
        }
    }
    return false;
}

